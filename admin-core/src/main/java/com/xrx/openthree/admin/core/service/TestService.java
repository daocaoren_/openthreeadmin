package com.xrx.openthree.admin.core.service;


import com.xrx.openthree.admin.dal.dao.UserMapper;
import com.xrx.openthree.admin.dal.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;


@Service
public class TestService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserMapper userMapper;

    public String test(){
        return stringRedisTemplate.opsForValue().get("springboot");
    }

    public User selectUserById(Long userId){
        return userMapper.selectByPrimaryKey(userId);
    }
}
