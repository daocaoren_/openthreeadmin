package com.xrx.openthree.admin.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

@Configuration
public class RedisConfig {

    /**
     * Type safe representation of application.properties
     */
    @Autowired
    private ClusterConfigurationProperties clusterProperties;

    public @Bean RedisConnectionFactory connectionFactory() {
        RedisClusterConfiguration redisClusterConfiguration=new RedisClusterConfiguration(clusterProperties.getNodes());
        JedisConnectionFactory jedisConnectionFactory=new JedisConnectionFactory(redisClusterConfiguration);
        jedisConnectionFactory.setPassword(clusterProperties.getPassword());
        return jedisConnectionFactory;
    }
}
