package com.xrx.openthree.admin.web.controller;

import com.xrx.openthree.admin.core.service.TestService;
import com.xrx.openthree.admin.dal.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping("/springboot")
    public String springBoot(){
        Long userId=262977394019663872L;
        User user=testService.selectUserById(userId);
        return user.getName();
    }
}
